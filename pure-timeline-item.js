import { LitElement, html, css } from 'lit-element';

class PureTimelineItem extends LitElement {
  static get properties() {
    return { 
      boxtext: { type: String }
    };
  }
  static get styles() {
    return css`
    :host {
      padding-top: 16px;
      display: block;
      --dots: #3F51B5;
      --box: #FF5722;
      --box-text: #FFFFFF;
      --item-background: #f5f5f5;
    }

    :host([hidden]) {
      display: none;
    }
    
    /* Item-Dot */
    :host::before {
      content: "";
      width: 30px;
      height: 30px;
      background: var(--dots);
      border-radius: 50%;
      position: absolute;
      left: 50%;
      margin-top: 25px;
      margin-left: -15px;
      left: 50px;
    }
    
    /* Left arrow */
    .content::after {
      content: "";
      position: absolute;
      border-style: solid;
      width: 0;
      height: 0;
      top: 30px;
      left: -15px;
      border-width: 10px 15px 10px 0;
      border-color: transparent var(--item-background) transparent transparent;
    }
    
    .content {
      position: relative;
      float: none;
      padding: 40px 30px 10px 30px;
      border-radius: 4px;
      background: var(--item-background);
      right: auto;
      left: 0;
      max-width: 100%;
      width: auto;
      margin-left: 70px;
    }
    
    .box {
      background: var(--box);
      display: inline-block;
      color: var(--box-text);
      padding: 10px;
      position: absolute;
      top: 0;
      right: 0;
      border-radius: 0 4px 0 0;
    }`;
  }
  render() {
    return html`<div class="content">${this.boxtext? html`<div class="box">${this.boxtext}</div>` : html`` }<slot></slot></div>`;
  }
}

customElements.define('pure-timeline-item', PureTimelineItem);