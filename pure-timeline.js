import { LitElement, html, css } from 'lit-element';

class PureTimeline extends LitElement {
  static get styles() {
    return css`
    :host {
      display: block;
      max-width: 100%;
      position: relative;
      padding-left: 15px;
      --timeline-bar: #C5CAE9;
    }

    :host([hidden]) {
      display: none;
    }  
    
    /* Left-Bar */
    :host::before {
      content: "";
      background: var(--timeline-bar);
      width: 5px;
      height: 140%;
      position: absolute;
      left: 50px;
      transform: translate(-55%, -30%);
    }`;
  }
  render() {
    return html`<slot></slot>`;
  }
}

customElements.define('pure-timeline', PureTimeline);