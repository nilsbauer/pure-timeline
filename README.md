# Pure Timeline
Timeline element built with lit-element based on [Sava Lazic's pen](https://codepen.io/savalazic/pen/QKwERN/)

![Timeline in use](https://gitlab.com/nilsbauer/pure-timeline/raw/master/img/image1.png?raw=true "pure-timeline")

## Installation

For setup instructions see [lit element's documentation](https://lit-element.polymer-project.org/guide/use).

## Usage
<!--
```
<custom-element-demo>
  <template>
    <script type="module" src="pure-timeline.js"></script>
    <script type="module" src="pure-timeline-item.js"></script>
    <next-code-block></next-code-block>
  </template>
</custom-element-demo>
```
-->

```html
  <pure-timeline>
    <pure-timeline-item boxtext="Test">
        Item 1
    </pure-timeline-item>
    <pure-timeline-item>
        Item 2 without box
    </pure-timeline-item>
    <pure-timeline-item boxtext="15.09.2019">
      <h1>Item 3</h1>
      <p>With content</p>
    </pure-timeline-item>
  </pure-timeline>
```

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes (don't forget to sign off): `git commit -sam 'Add some feature'` 
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits
It's based on [Sava Lazic's pen](https://codepen.io/savalazic/pen/QKwERN/).

## License
Copyright 2019 Nils Bauer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.